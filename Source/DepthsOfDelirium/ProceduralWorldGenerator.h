// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RoomInfo.h"
#include "BaseShapeComponent.h"
#include "ProceduralWorldGenerator.generated.h"

UCLASS()
class DEPTHSOFDELIRIUM_API AProceduralWorldGenerator : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AProceduralWorldGenerator();

	enum CompassDirection { None = 0, North, East, South, West };

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, Category = "Tile")
		TSubclassOf<class AActor> StartTileClass;

	UPROPERTY(EditAnywhere, Category = "Tile")
		TSubclassOf<class AActor> EndTileClass;

	UPROPERTY(EditAnywhere, Category = "Tile")
		TSubclassOf<class AActor> KeyTileClass;

	UPROPERTY(EditAnywhere, Category = "Tile")
		TSubclassOf<class AActor> DeadEndTileClass;

	UPROPERTY(EditAnywhere, Category = "Tile")
	TArray<TSubclassOf<ATile>> TileClasses;

	UPROPERTY()
	TArray<ATile*> Tileset;

	UPROPERTY()
	ATile* StartTile;

	UPROPERTY()
	UBaseShapeComponent* BaseShape;

	UPROPERTY()
	TArray<UBaseShapeComponent*> BaseShapeComponents;

	UPROPERTY()
	UBaseShapeComponent* CurrentShape;

	UPROPERTY()
		UBaseShapeComponent* PreviousShape;

	UPROPERTY()
		UBaseShapeComponent* CurrentConnectorTile;

	UPROPERTY()
	ATile* CurrentTileClone;

	UPROPERTY()
	bool CurrentTileLoop;

private:

	//FRoomInfo GetRandomTile(int x, int y);
	void RotateTile(ATile* tile);
	//void GenerateMap();
	//bool IsTileCompatible(FRoomInfo &tile, int xindex, int yindex);
	//bool IsTileCompatibleAllOrientations(FRoomInfo &tile, int xindex, int yindex);
	void RotateBaseShapeComponent(UBaseShapeComponent* component, CompassDirection fromDirection, TArray<UBaseShapeComponent*> &rotated);
	UBaseShapeComponent* CloneBaseShapeComponent(UBaseShapeComponent* component, UBaseShapeComponent* previousComponent, CompassDirection fromDirection);
	void FindConnectorComponents(TArray<UBaseShapeComponent*> &outArray, UBaseShapeComponent* baseComponent, CompassDirection fromDirection, TArray<UBaseShapeComponent*> &checked);
	template<class T> void Shuffle(TArray<T> &arrayToShuffle);
	void PlaceInnerTile(UBaseShapeComponent* component, ATile* room, CompassDirection fromDirection);
	ATile* CloneRoomInfo(ATile* room);
	void FindCompatibleTile(ATile* room, CompassDirection direction);
	void SpawnTile(int x, int y, ATile* tile);
};
