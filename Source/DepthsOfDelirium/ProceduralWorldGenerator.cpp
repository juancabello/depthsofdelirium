// Fill out your copyright notice in the Description page of Project Settings.

#include "ProceduralWorldGenerator.h"
#include "BaseShapeComponent.h"
#include "Engine/World.h"
#include "Misc/MessageDialog.h"

// Sets default values
AProceduralWorldGenerator::AProceduralWorldGenerator()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AProceduralWorldGenerator::BeginPlay()
{
	Super::BeginPlay();

	// Create the first base shape
	UBaseShapeComponent* component1 = NewObject<UBaseShapeComponent>();
	component1->bCanConnectNorth = true;
	component1->bCanConnectSouth = true;

	// Create the second base shape
	UBaseShapeComponent* component2_1 = NewObject<UBaseShapeComponent>();
	component2_1->bCanConnectSouth = true;
	UBaseShapeComponent* component2 = NewObject<UBaseShapeComponent>();
	component2->bCanConnectNorth = true;
	component2->SouthTile = component2_1;
	component2_1->NorthTile = component2;
	
	// Create the third base shape
	UBaseShapeComponent* component3_1 = NewObject<UBaseShapeComponent>();
	component3_1->bCanConnectNorth = true;
	UBaseShapeComponent* component3_2 = NewObject<UBaseShapeComponent>();
	component3_2->bCanConnectSouth = true;
	UBaseShapeComponent* component3 = NewObject<UBaseShapeComponent>();
	component3->NorthTile = component3_1;
	component3_1->SouthTile = component3;
	component3->SouthTile = component3_2;
	component3_2->NorthTile = component3;

	// Create the fourth base shape
	UBaseShapeComponent* component4_1 = NewObject<UBaseShapeComponent>();
	component4_1->bCanConnectEast = true;
	UBaseShapeComponent* component4_2 = NewObject<UBaseShapeComponent>();
	component4_2->bCanConnectSouth = true;
	UBaseShapeComponent* component4 = NewObject<UBaseShapeComponent>();
	component4->EastTile = component4_1;
	component4_1->WestTile = component4;
	component4->SouthTile = component4_2;
	component4_2->NorthTile = component4;

	// Create the fifth base shape
	UBaseShapeComponent* component5_1 = NewObject<UBaseShapeComponent>();
	component5_1->bCanConnectNorth = true;
	component5_1->bCanConnectEast = true;
	UBaseShapeComponent* component5_2 = NewObject<UBaseShapeComponent>();
	component5_2->bCanConnectEast = true;
	component5_2->bCanConnectSouth = true;
	UBaseShapeComponent* component5_3 = NewObject<UBaseShapeComponent>();
	component5_3->bCanConnectSouth = true;
	component5_3->bCanConnectWest = true;
	UBaseShapeComponent* component5 = NewObject<UBaseShapeComponent>();
	component5->bCanConnectWest = true;
	component5->bCanConnectNorth = true;
	component5->EastTile = component5_1;
	component5_1->WestTile = component5;
	component5_1->SouthTile = component5_2;
	component5_2->NorthTile = component5_1;
	component5_2->EastTile = component5_3;
	component5_3->WestTile = component5_2;
	component5_3->NorthTile = component5;
	component5->SouthTile = component5_3;

	// Add the components to a list
	BaseShapeComponents.Add(component1);
	BaseShapeComponents.Add(component2);
	BaseShapeComponents.Add(component3);
	BaseShapeComponents.Add(component4);
	//BaseShapeComponents.Add(component5);

	// Choose how many components the base shape will have
	int numberOfComponents = rand() % 5 + 3; // 5-7 components

	// Choose a random shape and clone it
	int shapeIndex = rand() % BaseShapeComponents.Num();
	BaseShape = CloneBaseShapeComponent(BaseShapeComponents[shapeIndex], NULL, None);

	// Rotate the shape randomly
	int baseShapeRotation = rand() % 4;
	for (int i = 0; i < baseShapeRotation; i++)
	{
		TArray<UBaseShapeComponent*> rotated = TArray<UBaseShapeComponent*>();
		RotateBaseShapeComponent(BaseShape, None, rotated);
	}

	// Get the connectors for this shape
	TArray<UBaseShapeComponent*> baseShapeConnectorComponents;
	TArray<UBaseShapeComponent*> checked;
	FindConnectorComponents(baseShapeConnectorComponents, BaseShape, None, checked);

	// Select a random connector tile
	int connectorIndex = rand() % baseShapeConnectorComponents.Num();
	UBaseShapeComponent* baseConnectorTile = baseShapeConnectorComponents[connectorIndex];

	PreviousShape = BaseShape;

	// Iterate through the remaining components
	for (int i = 1; i < numberOfComponents; i++)
	{
		// Choose a random shape and clone it
		int shapeIndex = rand() % BaseShapeComponents.Num();
		CurrentShape = CloneBaseShapeComponent(BaseShapeComponents[shapeIndex], NULL, None);

		// Rotate the shape randomly
		int shapeRotation = rand() % 4;
		for (int i = 0; i < shapeRotation; i++)
		{
			TArray<UBaseShapeComponent*> rotated = TArray<UBaseShapeComponent*>();
			RotateBaseShapeComponent(CurrentShape, None, rotated);
		}

		CurrentConnectorTile = nullptr;
		if(i > 1)
		{
			// Find connectors on the previous shape
			TArray<UBaseShapeComponent*> connectorComponents = TArray<UBaseShapeComponent*>();
			TArray<UBaseShapeComponent*> checked2 = TArray<UBaseShapeComponent*>();
			FindConnectorComponents(connectorComponents, PreviousShape, None, checked2);

			// Shuffle the connector tiles
			Shuffle(connectorComponents);

			// Find the first available valid connector tile
			for (UBaseShapeComponent* component : connectorComponents)
			{
				if (component->bCanConnectNorth && !component->NorthTile ||
					component->bCanConnectEast && !component->EastTile ||
					component->bCanConnectSouth && !component->SouthTile ||
					component->bCanConnectWest && !component->WestTile)
				{
					CurrentConnectorTile = component;
					break;
				}
			}
		}
		else
		{
			CurrentConnectorTile = baseConnectorTile;
		}

		// Get the direction of the connecting edges
		TArray<int> connectableDirections;
		
		if (CurrentConnectorTile->bCanConnectNorth)
			connectableDirections.Add(1);
		if (CurrentConnectorTile->bCanConnectEast)
			connectableDirections.Add(2);
		if (CurrentConnectorTile->bCanConnectSouth)
			connectableDirections.Add(3);
		if (CurrentConnectorTile->bCanConnectWest)
			connectableDirections.Add(4);

		// Shuffle the directions array
		Shuffle(connectableDirections);

		// Get the connectors for this shape
		int connectionDirection = connectableDirections[0];
		bool isConnected = false;
		TArray<UBaseShapeComponent*> newConnectorComponents = TArray<UBaseShapeComponent*>();
		TArray<UBaseShapeComponent*> checked3 = TArray<UBaseShapeComponent*>();
		FindConnectorComponents(newConnectorComponents, CurrentShape, None, checked3);

		// Connect the new component to the base shape
		while (!isConnected)
		{
			for (UBaseShapeComponent* component : newConnectorComponents)
			{
				switch (connectionDirection)
				{
				case 1:
					if (component->bCanConnectSouth)
					{
						CurrentConnectorTile->NorthTile = component;
						component->SouthTile = CurrentConnectorTile;
						isConnected = true;
					}
					break;
				case 2:
					if (component->bCanConnectWest)
					{
						CurrentConnectorTile->EastTile = component;
						component->WestTile = CurrentConnectorTile;
						isConnected = true;
					}
					break;
				case 3:
					if (component->bCanConnectNorth)
					{
						CurrentConnectorTile->SouthTile = component;
						component->NorthTile = CurrentConnectorTile;
						isConnected = true;
					}
					break;
				case 4:
					if (component->bCanConnectEast)
					{
						CurrentConnectorTile->WestTile = component;
						component->EastTile = CurrentConnectorTile;
						isConnected = true;
					}
					break;
				}
			}

			// If no connection was made, rotate for next loop
			if (!isConnected)
			{
				TArray<UBaseShapeComponent*> rotated2 = TArray<UBaseShapeComponent*>();
				RotateBaseShapeComponent(CurrentShape, None, rotated2);
			}
		}

		PreviousShape = CurrentShape;
	}

	// Place inner tiles
	StartTile = NewObject<ATile>();
	StartTile->bNorthDoor = true;
	StartTile->bEastDoor = true;
	StartTile->bSouthDoor = true;
	StartTile->bWestDoor = true;

	PlaceInnerTile(BaseShape, StartTile, None);
	SpawnTile(0, 0, StartTile);
}

void AProceduralWorldGenerator::SpawnTile(int x, int y, ATile* tile)
{
	// Rotate the tile and place in center of world
	//FVector location(0.0f, 0.0f, 0.0f);
	//FRotator rotation(0.0f, tile->Rotation, 0.0f);
	//FActorSpawnParameters spawnInfo;
	
	// Spawn the tile
	//AActor* room = GetWorld()->SpawnActor<AActor>(tile->GetClass(), location, rotation, spawnInfo);
	
	// Move the tile to its position in the world
	FVector origin(0.0f, 0.0f, 0.0f);
	FVector box(0.0f, 0.0f, 0.0f);
	tile->GetActorBounds(false, origin, box);
	tile->SetActorLocation(FVector(x * box.X * 2, y * box.Y * 2, 0.0f));
	//tile->SetActorLocation(FVector(x * 500.0f, y * 500.0f, 0.0f));
	tile->SetActorRotation(FRotator(0.0f, tile->Rotation, 0.0f));
	tile->bSpawned = true;

	// Spawn the adjacent tiles
	if (tile->NorthTile && !tile->NorthTile->bSpawned)
		SpawnTile(x, y - 1, tile->NorthTile);
	if (tile->EastTile && !tile->EastTile->bSpawned)
		SpawnTile(x + 1, y, tile->EastTile);
	if (tile->SouthTile && !tile->SouthTile->bSpawned)
		SpawnTile(x, y + 1, tile->SouthTile);
	if (tile->WestTile && !tile->WestTile->bSpawned)
		SpawnTile(x - 1, y, tile->WestTile);
}

//bool AProceduralWorldGenerator::IsTileCompatible(FRoomInfo &tile, int x, int y)
//{
//	// Check north
//	if (y - 1 >= 0 && Map[x][y - 1].TileClass && Map[x][y - 1].bSouthDoor)
//	{
//		if (!tile.bNorthDoor)
//			return false;
//	}
//	
//	// Check south
//	if (y + 1 < 7 && Map[x][y + 1].TileClass && Map[x][y + 1].bNorthDoor)
//	{
//		if (!tile.bSouthDoor)
//			return false;
//	}
//
//	// Check east
//	if (x + 1 < 7 && Map[x + 1][y].TileClass && Map[x + 1][y].bEastDoor)
//	{
//		if (!tile.bWestDoor)
//			return false;
//	}
//
//	// Check west
//	if (x - 1 >= 0 && Map[x - 1][y].TileClass && Map[x - 1][y].bWestDoor)
//	{
//		if (!tile.bEastDoor)
//			return false;
//	}
//
//	return true;
//	return true;
//}
//
//void AProceduralWorldGenerator::GenerateMap()
//{
//	for (int x = 0; x < 7; x++)
//	{
//		for (int y = 0; y < 7; y++)
//		{
//			// Only spawn the tile if there is meant to be one in this spot
//			if (Map[x][y].TileClass)
//			{
//				int xoff = x - 3;
//				int yoff = y - 3;
//
//				// Rotate the tile and place in center of world
//				FVector location(0.0f, 0.0f, 0.0f);
//				FRotator rotation(0.0f, Map[x][y].Rotation, 0.0f);
//				FActorSpawnParameters spawnInfo;
//
//				// Spawn the tile
//				AActor* room = GetWorld()->SpawnActor<AActor>(Map[x][y].TileClass, location, rotation, spawnInfo);
//
//				// Move the tile to its position in the world
//				FVector origin(0.0f, 0.0f, 0.0f);
//				FVector box(0.0f, 0.0f, 0.0f);
//				room->GetActorBounds(false, origin, box);
//				room->SetActorLocation(FVector(xoff * box.X * 2, yoff * box.Y * 2, 0.0f));
//			}
//		}
//	}
//}
//
//FRoomInfo AProceduralWorldGenerator::GetRandomTile(int x, int y)
//{
//	TArray<int> indices;
//	for (int i = 0; i < Tiles.Num(); i++)
//		indices.Add(i);
//
//	while (indices.Num() > 0)
//	{
//		// Get a random index from the indices array
//		int maxIndex = indices.Num() - 1;
//		int randomIndex = rand() % maxIndex;
//		int index = indices[randomIndex];
//
//		// Grab the tile at that index and check for compatibility
//		FRoomInfo tile = Tiles[index];
//		if (IsTileCompatibleAllOrientations(tile, x, y))
//			return tile;
//		else
//			indices.Remove(index);
//	}
//
//	// If we got here, there are no compatible tiles in the tileset
//	// First we will try a deadend tile
//	FRoomInfo deadEndTile = FRoomInfo(DeadEndTileClass, false, true, false, false);
//	if (IsTileCompatibleAllOrientations(deadEndTile, x, y))
//		return deadEndTile;
//
//	// No tiles are compatible, place a 4 way
//	FRoomInfo startTile = FRoomInfo(StartTileClass, true, true, true, true);
//	return startTile;
//}
//
//bool AProceduralWorldGenerator::IsTileCompatibleAllOrientations(FRoomInfo &tile, int xindex, int yindex)
//{
//	if (IsTileCompatible(tile, xindex, yindex))
//		return true;
//
//	// If it wasn't compatible, see if it is after rotating
//	for (int i = 0; i < 3; i++)
//	{
//		RotateTile(tile);
//		if (IsTileCompatible(tile, xindex, yindex))
//			return true;
//	}
//
//	// No compatibility found in any orientation
//	return false;
//}

void AProceduralWorldGenerator::RotateTile(ATile* tile)
{
	// Save the orientation
	bool northDoor = tile->bNorthDoor;
	bool eastDoor = tile->bEastDoor;
	bool southDoor = tile->bSouthDoor;
	bool westDoor = tile->bWestDoor;

	// Rotate the tile
	tile->bNorthDoor = westDoor;
	tile->bEastDoor = northDoor;
	tile->bSouthDoor = eastDoor;
	tile->bWestDoor = southDoor;
	tile->Rotation += 90.0f;

	// Get a reference to the door we need to match
	/*bool fallback = true;
	bool* door;
	switch (direction)
	{
	case 0:
		door = &tile.bNorthDoor;
		break;
	case 1:
		door = &tile.bEastDoor;
		break;
	case 2:
		door = &tile.bSouthDoor;
		break;
	case 3:
		door = &tile.bWestDoor;
		break;
	default:
		door = &fallback;
		break;
	}

	while (!door)
	{
		// Save the orientation
		bool northDoor = tile.bNorthDoor;
		bool eastDoor = tile.bEastDoor;
		bool southDoor = tile.bSouthDoor;
		bool westDoor = tile.bWestDoor;

		// Rotate the tile
		tile.bNorthDoor = westDoor;
		tile.bEastDoor = northDoor;
		tile.bSouthDoor = eastDoor;
		tile.bWestDoor = southDoor;
		tile.Rotation += 90.0f;
	}*/
}

void AProceduralWorldGenerator::RotateBaseShapeComponent(UBaseShapeComponent *component, CompassDirection fromDirection, TArray<UBaseShapeComponent*> &rotated)
{
	if (rotated.Contains(component))
		return;

	bool n = component->bCanConnectNorth;
	bool e = component->bCanConnectEast;
	bool s = component->bCanConnectSouth;
	bool w = component->bCanConnectWest;

	UBaseShapeComponent& cn = *component->NorthTile;
	UBaseShapeComponent& ce = *component->EastTile;
	UBaseShapeComponent& cs = *component->SouthTile;
	UBaseShapeComponent& cw = *component->WestTile;

	component->bCanConnectNorth = e;
	component->bCanConnectEast = s;
	component->bCanConnectSouth = w;
	component->bCanConnectWest = n;

	component->NorthTile = &ce;
	component->EastTile = &cs;
	component->SouthTile = &cw;
	component->WestTile = &cn;

	rotated.Add(component);

	if (fromDirection != North && component->NorthTile)
		RotateBaseShapeComponent(component->NorthTile, South, rotated);

	if (fromDirection != South && component->SouthTile)
		RotateBaseShapeComponent(component->SouthTile, North, rotated);

	if (fromDirection != East && component->EastTile)
		RotateBaseShapeComponent(component->EastTile, West, rotated);

	if (fromDirection != West && component->WestTile)
		RotateBaseShapeComponent(component->WestTile, East, rotated);
}

UBaseShapeComponent* AProceduralWorldGenerator::CloneBaseShapeComponent(UBaseShapeComponent* component, UBaseShapeComponent* previousComponent, CompassDirection fromDirection)
{
	UBaseShapeComponent* newComponent = NewObject<UBaseShapeComponent>();
	newComponent->bCanConnectNorth = component->bCanConnectNorth;
	newComponent->bCanConnectEast = component->bCanConnectEast;
	newComponent->bCanConnectSouth = component->bCanConnectSouth;
	newComponent->bCanConnectWest = component->bCanConnectWest;

	switch (fromDirection)
	{
	case South:
		previousComponent->NorthTile = newComponent;
		newComponent->SouthTile = previousComponent;
		if (previousComponent->EastTile)
			newComponent->EastTile = previousComponent->EastTile->NorthTile;
		if (previousComponent->WestTile)
			newComponent->WestTile = previousComponent->WestTile->NorthTile;
		if (newComponent->EastTile && newComponent->EastTile->NorthTile && newComponent->EastTile->NorthTile->WestTile)
			newComponent->NorthTile = newComponent->EastTile->NorthTile->WestTile;
		if (newComponent->WestTile && newComponent->WestTile->NorthTile && newComponent->WestTile->NorthTile->EastTile)
			newComponent->NorthTile = newComponent->WestTile->NorthTile->EastTile;
		break;
	case West:
		previousComponent->EastTile = newComponent;
		newComponent->WestTile = previousComponent;
		if (previousComponent->NorthTile)
			newComponent->NorthTile = previousComponent->NorthTile->EastTile;
		if (previousComponent->SouthTile)
			newComponent->SouthTile = previousComponent->SouthTile->EastTile;
		if (newComponent->NorthTile && newComponent->NorthTile->EastTile && newComponent->NorthTile->EastTile->SouthTile)
			newComponent->WestTile = newComponent->NorthTile->EastTile->SouthTile;
		if (newComponent->SouthTile && newComponent->SouthTile->EastTile && newComponent->SouthTile->EastTile->NorthTile)
			newComponent->WestTile = newComponent->SouthTile->EastTile->NorthTile;
		break;
	case North:
		previousComponent->SouthTile = newComponent;
		newComponent->NorthTile = previousComponent;
		if (previousComponent->EastTile)
			newComponent->EastTile = previousComponent->EastTile->SouthTile;
		if (previousComponent->WestTile)
			newComponent->WestTile = previousComponent->WestTile->SouthTile;
		if (newComponent->EastTile && newComponent->EastTile->SouthTile && newComponent->EastTile->SouthTile->WestTile)
			newComponent->SouthTile = newComponent->EastTile->SouthTile->WestTile;
		if (newComponent->WestTile && newComponent->WestTile->SouthTile && newComponent->WestTile->SouthTile->EastTile)
			newComponent->SouthTile = newComponent->WestTile->SouthTile->EastTile;
		break;
	case East:
		previousComponent->WestTile = newComponent;
		newComponent->EastTile = previousComponent;
		if (previousComponent->NorthTile)
			newComponent->NorthTile = previousComponent->NorthTile->WestTile;
		if (previousComponent->SouthTile)
			newComponent->SouthTile = previousComponent->SouthTile->WestTile;
		if (newComponent->NorthTile && newComponent->NorthTile->WestTile && newComponent->NorthTile->WestTile->SouthTile)
			newComponent->WestTile = newComponent->NorthTile->WestTile->SouthTile;
		if (newComponent->SouthTile && newComponent->SouthTile->WestTile && newComponent->SouthTile->WestTile->NorthTile)
			newComponent->WestTile = newComponent->SouthTile->WestTile->NorthTile;
		break;
	}

	if (!newComponent->NorthTile && component->NorthTile)
	{
		UBaseShapeComponent* northTile = CloneBaseShapeComponent(component->NorthTile, component, South);
		newComponent->NorthTile = northTile;
	}
	if (!newComponent->EastTile && component->EastTile)
	{
		UBaseShapeComponent* eastTile = CloneBaseShapeComponent(component->EastTile, component, West);
		newComponent->EastTile = eastTile;
	}
	if (!newComponent->SouthTile && component->SouthTile)
	{
		UBaseShapeComponent* southTile = CloneBaseShapeComponent(component->SouthTile, component, North);
		newComponent->SouthTile = southTile;
	}
	if (!newComponent->WestTile && component->WestTile)
	{
		UBaseShapeComponent* westTile = CloneBaseShapeComponent(component->WestTile, component, East);
		newComponent->WestTile = westTile;
	}

	return newComponent;
}

ATile* AProceduralWorldGenerator::CloneRoomInfo(ATile* room)
{
	FActorSpawnParameters spawnParams = FActorSpawnParameters();
	//spawnParams.Template = room;

	ATile* newRoom = GetWorld()->SpawnActor<ATile>(room->GetClass(), spawnParams);
	newRoom->bNorthDoor = room->bNorthDoor;
	newRoom->bEastDoor = room->bEastDoor;
	newRoom->bSouthDoor = room->bSouthDoor;
	newRoom->bWestDoor = room->bWestDoor;
	//newRoom->TileClass = room->TileClass;

	return newRoom;
}

void AProceduralWorldGenerator::FindConnectorComponents(TArray<UBaseShapeComponent*> &outArray, UBaseShapeComponent* baseComponent, CompassDirection fromDirection, TArray<UBaseShapeComponent*> &checked)
{
	checked.Add(baseComponent);

	if (baseComponent->bCanConnectNorth || baseComponent->bCanConnectEast || baseComponent->bCanConnectSouth || baseComponent->bCanConnectWest)
		outArray.Add(baseComponent);

	if (fromDirection != North && baseComponent->NorthTile && !checked.Contains(baseComponent->NorthTile))
		FindConnectorComponents(outArray, baseComponent->NorthTile, South, checked);

	if (fromDirection != East && baseComponent->EastTile && !checked.Contains(baseComponent->EastTile))
		FindConnectorComponents(outArray, baseComponent->EastTile, West, checked);

	if (fromDirection != South && baseComponent->SouthTile && !checked.Contains(baseComponent->SouthTile))
		FindConnectorComponents(outArray, baseComponent->SouthTile, North, checked);

	if (fromDirection != West && baseComponent->WestTile && !checked.Contains(baseComponent->WestTile))
		FindConnectorComponents(outArray, baseComponent->WestTile, East, checked);
}

void AProceduralWorldGenerator::PlaceInnerTile(UBaseShapeComponent* component, ATile* room, CompassDirection fromDirection = None)
{
	if (component->NorthTile && !room->NorthTile && fromDirection != North)
	{
		FindCompatibleTile(room, North);
		PlaceInnerTile(component->NorthTile, room->NorthTile, South);
	}

	if (component->EastTile && !room->EastTile && fromDirection != East)
	{
		FindCompatibleTile(room, East);
		PlaceInnerTile(component->EastTile, room->EastTile, West);
	}

	if (component->SouthTile && !room->SouthTile && fromDirection != South)
	{
		FindCompatibleTile(room, South);
		PlaceInnerTile(component->SouthTile, room->SouthTile, North);
	}

	if (component->WestTile && !room->WestTile && fromDirection != West)
	{
		FindCompatibleTile(room, West);
		PlaceInnerTile(component->WestTile, room->WestTile, East);
	}
}

// Called every frame
void AProceduralWorldGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

template<class T> void AProceduralWorldGenerator::Shuffle(TArray<T> &arrayToShuffle)
{
	for (int i = arrayToShuffle.Num() - 1; i > 0; i--)
	{
		int j = FMath::FloorToInt(FMath::Rand() * (i + 1)) % arrayToShuffle.Num();
		T* temp = &arrayToShuffle[i];
		arrayToShuffle[i] = arrayToShuffle[j];
		arrayToShuffle[j] = *temp;
	}
}

void AProceduralWorldGenerator::FindCompatibleTile(ATile* room, CompassDirection direction)
{
	// Create a room and attach it
	ATile* tile = NewObject<ATile>();

	switch (direction)
	{
	case North:
		room->NorthTile = tile;
		tile->SouthTile = room;
		if (room->EastTile)
			tile->EastTile = room->EastTile->NorthTile;
		if (room->WestTile)
			tile->WestTile = room->WestTile->NorthTile;
		if (tile->EastTile && tile->EastTile->NorthTile && tile->EastTile->NorthTile->WestTile)
			tile->NorthTile = tile->EastTile->NorthTile->WestTile;
		if (tile->WestTile && tile->WestTile->NorthTile && tile->WestTile->NorthTile->EastTile)
			tile->NorthTile = tile->WestTile->NorthTile->EastTile;
		break;
	case East:
		room->EastTile = tile;
		tile->WestTile = room;
		if (room->NorthTile)
			tile->NorthTile = room->NorthTile->EastTile;
		if (room->SouthTile)
			tile->SouthTile = room->SouthTile->EastTile;
		if (tile->NorthTile && tile->NorthTile->EastTile && tile->NorthTile->EastTile->SouthTile)
			tile->EastTile = tile->NorthTile->EastTile->SouthTile;
		if (tile->SouthTile && tile->SouthTile->EastTile && tile->SouthTile->EastTile->NorthTile)
			tile->EastTile = tile->SouthTile->EastTile->NorthTile;
		break;
	case South:
		room->SouthTile = tile;
		tile->NorthTile = room;
		if (room->EastTile)
			tile->EastTile = room->EastTile->SouthTile;
		if (room->WestTile)
			tile->WestTile = room->WestTile->SouthTile;
		if (tile->EastTile && tile->EastTile->SouthTile && tile->EastTile->SouthTile->WestTile)
			tile->SouthTile = tile->EastTile->SouthTile->WestTile;
		if (tile->WestTile && tile->WestTile->SouthTile && tile->WestTile->SouthTile->EastTile)
			tile->SouthTile = tile->WestTile->SouthTile->EastTile;
		break;
	case West:
		room->WestTile = tile;
		tile->EastTile = room;
		if (room->NorthTile)
			tile->NorthTile = room->NorthTile->WestTile;
		if (room->SouthTile)
			tile->SouthTile = room->SouthTile->WestTile;
		if (tile->NorthTile && tile->NorthTile->WestTile && tile->NorthTile->WestTile->SouthTile)
			tile->WestTile = tile->NorthTile->WestTile->SouthTile;
		if (tile->SouthTile && tile->SouthTile->WestTile && tile->SouthTile->WestTile->NorthTile)
			tile->WestTile = tile->SouthTile->WestTile->NorthTile;
		break;
	}

	// Populate the tile array if it isn't already
	if (Tileset.Num() < 1)
	{
		for (TSubclassOf<ATile> tileClass : TileClasses)
		{
			FVector Location(0.0f, 0.0f, 0.0f);
			FRotator Rotation(0.0f, 0.0f, 0.0f);
			FActorSpawnParameters SpawnInfo;
			Tileset.Add(GetWorld()->SpawnActor<ATile>(tileClass, Location, Rotation, SpawnInfo));
		}
	}

	// Copy the tile array
	TArray<ATile*> tiles;
	for (ATile* roomInfo : Tileset)
		tiles.Add(roomInfo);

	ATile* tileThatWasCloned = nullptr;

	CurrentTileLoop = false;
	while (!CurrentTileLoop)
	{
		if (tiles.Num() == 0)
		{
			FMessageDialog::Debugf(FText::FromString("FindCompatibleTile failed due to no tiles being compatible."));
			break; // Break out of the loop to stop locking up
		}

		// Shuffle the tiles and take a copy of the top tile
		Shuffle(tiles);
		tileThatWasCloned = tiles[0];
		CurrentTileClone = CloneRoomInfo(tiles[0]);

		// Rotate the tile randomly
		for (int i = 0; i < rand() % 4; i++)
			RotateTile(CurrentTileClone);

		for (int i = 0; i < 4; i++)
		{
			// Rotate the tile
			for (int j = 0; j < i; j++)
				RotateTile(CurrentTileClone);

			// Check if it fits
			bool isCompatible = true;
			if (tile->NorthTile && tile->NorthTile->bSouthDoor && !CurrentTileClone->bNorthDoor)
				isCompatible = false;
			if (tile->EastTile && tile->EastTile->bWestDoor && !CurrentTileClone->bEastDoor)
				isCompatible = false;
			if (tile->SouthTile && tile->SouthTile->bNorthDoor && !CurrentTileClone->bSouthDoor)
				isCompatible = false;
			if (tile->WestTile && tile->WestTile->bEastDoor && !CurrentTileClone->bWestDoor)
				isCompatible = false;

			// Add the tile
			if (isCompatible)
			{
				tile->bNorthDoor = CurrentTileClone->bNorthDoor;
				tile->bEastDoor = CurrentTileClone->bEastDoor;
				tile->bSouthDoor = CurrentTileClone->bSouthDoor;
				tile->bWestDoor = CurrentTileClone->bWestDoor;
				tile->Rotation = CurrentTileClone->Rotation;

				ATile* oldTile = tile;

				FActorSpawnParameters spawnParams = FActorSpawnParameters();
				spawnParams.Template = CurrentTileClone;

				tile = GetWorld()->SpawnActor<ATile>(CurrentTileClone->GetClass(), spawnParams);
				tile->bNorthDoor = oldTile->bNorthDoor;
				tile->bEastDoor = oldTile->bEastDoor;
				tile->bSouthDoor = oldTile->bSouthDoor;
				tile->bWestDoor = oldTile->bWestDoor;
				tile->NorthTile = oldTile->NorthTile;
				tile->EastTile = oldTile->EastTile;
				tile->SouthTile = oldTile->SouthTile;
				tile->WestTile = oldTile->WestTile;

				switch (direction)
				{
				case North:
					room->NorthTile = tile;
					tile->SouthTile = room;
					if (room->EastTile)
						tile->EastTile = room->EastTile->NorthTile;
					if (room->WestTile)
						tile->WestTile = room->WestTile->NorthTile;
					if (tile->EastTile && tile->EastTile->NorthTile && tile->EastTile->NorthTile->WestTile)
						tile->NorthTile = tile->EastTile->NorthTile->WestTile;
					if (tile->WestTile && tile->WestTile->NorthTile && tile->WestTile->NorthTile->EastTile)
						tile->NorthTile = tile->WestTile->NorthTile->EastTile;
					break;
				case East:
					room->EastTile = tile;
					tile->WestTile = room;
					if (room->NorthTile)
						tile->NorthTile = room->NorthTile->EastTile;
					if (room->SouthTile)
						tile->SouthTile = room->SouthTile->EastTile;
					if (tile->NorthTile && tile->NorthTile->EastTile && tile->NorthTile->EastTile->SouthTile)
						tile->EastTile = tile->NorthTile->EastTile->SouthTile;
					if (tile->SouthTile && tile->SouthTile->EastTile && tile->SouthTile->EastTile->NorthTile)
						tile->EastTile = tile->SouthTile->EastTile->NorthTile;
					break;
				case South:
					room->SouthTile = tile;
					tile->NorthTile = room;
					if (room->EastTile)
						tile->EastTile = room->EastTile->SouthTile;
					if (room->WestTile)
						tile->WestTile = room->WestTile->SouthTile;
					if (tile->EastTile && tile->EastTile->SouthTile && tile->EastTile->SouthTile->WestTile)
						tile->SouthTile = tile->EastTile->SouthTile->WestTile;
					if (tile->WestTile && tile->WestTile->SouthTile && tile->WestTile->SouthTile->EastTile)
						tile->SouthTile = tile->WestTile->SouthTile->EastTile;
					break;
				case West:
					room->WestTile = tile;
					tile->EastTile = room;
					if (room->NorthTile)
						tile->NorthTile = room->NorthTile->WestTile;
					if (room->SouthTile)
						tile->SouthTile = room->SouthTile->WestTile;
					if (tile->NorthTile && tile->NorthTile->WestTile && tile->NorthTile->WestTile->SouthTile)
						tile->WestTile = tile->NorthTile->WestTile->SouthTile;
					if (tile->SouthTile && tile->SouthTile->WestTile && tile->SouthTile->WestTile->NorthTile)
						tile->WestTile = tile->SouthTile->WestTile->NorthTile;
					break;
				}

				//tile->TileClass = tileClone->TileClass;
				CurrentTileLoop = true;
			}
		}

		// If this tile wasn't compatible, remove it so it doesn't get used again
		if (!CurrentTileLoop)
			tiles.Remove(tileThatWasCloned);
		else
			break;
	}
}