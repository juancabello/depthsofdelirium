// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RoomInfo.generated.h"

UCLASS(BlueprintType)
class ATile : public AActor
{
	GENERATED_BODY()

public:

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile")
	//TSubclassOf<class AActor> TileClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile")
		bool bNorthDoor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile")
		bool bEastDoor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile")
		bool bSouthDoor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile")
		bool bWestDoor;

	UPROPERTY()
	ATile* NorthTile = nullptr;

	UPROPERTY()
	ATile* EastTile = nullptr;

	UPROPERTY()
	ATile* SouthTile = nullptr;

	UPROPERTY()
	ATile* WestTile = nullptr;

	float Rotation;
	bool bSpawned;
};